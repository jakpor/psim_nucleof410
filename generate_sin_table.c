/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdint.h>
#include <math.h>
#include <stdio.h>

#define PI 3.14159265

#define SIN_TABLE_LEN 32
#define DAC_PRECISION 12
#define DAC_MIN       (120)
#define DAC_MAX       (3637)
#define FFT_LEN (2048) //2048

uint16_t sin_table[32] = {0};

float DAC_value(float degree_rad)
{
  return (sin(degree_rad)+1)/2.0 * (DAC_MAX-DAC_MIN)+DAC_MIN;
}

void generate_sin_table()
{
  int i;
  for(i=0;i<SIN_TABLE_LEN;i++){
      //printf("%4.2f\n",DAC_value(2.0*PI*i/SIN_TABLE_LEN));
    sin_table[i] = ((uint16_t)DAC_value(2.0*PI*i/SIN_TABLE_LEN))&0xFFF;
    
  }
}

void print_table()
{
  int i;
  printf("uint16_t sin_table[32] = {"); 
  for(i=0;i<SIN_TABLE_LEN;i++){
    if (i%8 == 0 ){
      printf("\n");
    }
    printf("%4d, ",sin_table[i]);
  }
  printf("\n};\n");
}

int main()
{
    generate_sin_table();
    print_table();

    return 0;
}

