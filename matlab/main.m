%%
close all
clear all

%% Generate simulation params
model_bode_fft
clear all
%% Generate experiment params
experiment_fft
clear all
%% Load params
load sim_params.mat
load exp_params.mat

%% Actual obiect
K = 3;
R1 = 4700; C1 = 10e-6; R2 = 22000; C2 = 22e-6;
T1 = (R1*C1); f1 = 1/(2*pi*T1);
T2 = (R2*C2); f2 = 1/(2*pi*T2);

display(['f1_sim  = ',num2str(simulation.freq45), '  Hz']);
display(['f1_exp  = ',num2str(experiment.freq45), '  Hz']);
display(['f1_act  = ',num2str(f2), '  Hz']);
display(['f2_sim  = ',num2str(simulation.freq135), '  Hz']);
display(['f2_exp  = ',num2str(experiment.freq135), '  Hz']);
display(['f2_act  = ',num2str(f1), '  Hz']);
display(['Błąd względny: ', num2str((experiment.freq135-f1)/f1*100),'%, ', num2str((experiment.freq45-f2)/f2*100),'%']);

%% Wykres Bodego
F = figure();
subplot(2,1,1);
semilogx(simulation.freq, simulation.gain_log, 'bx', experiment.freq, experiment.gain_log, 'rx');
ylabel('20log_{10}(Y/U)');
title('Amplituda')
subplot(2,1,2)
semilogx(simulation.freq,simulation.phase_deg, 'bx',experiment.freq,experiment.phase_deg, 'rx');
title('Faza')
ylabel('Faza, stopnie (^o)');
xlabel('Czestotliwość, Hz');
legend('Symulacja','Eksperyment','Location','Best')

plot_png(F, 'wykres_bodego', [400,600]);
plot_pdf(F, 'wykres_bodego', [400,600]);

%% Wykres - symulacja vs bode
NUM = 1;
DEN = [T1*T2, T1+T2, 1];
SYS = tf(NUM,DEN);
[MAG,PHASE] = bode(SYS,simulation.freq*2*pi);

MAG = reshape(MAG,[1,50]);
PHASE = reshape(PHASE,[1,50]);

F = figure(1);
subplot(2,1,1);
semilogx( simulation.freq, 20*log10(MAG), 'r', simulation.freq, simulation.gain_log, 'bx', experiment.freq, experiment.gain_log, 'gx');
ylabel('20log_{10}(Y/U)');
title('Amplituda')
xlim([min(simulation.freq), max(simulation.freq)]);
legend('Funkcja Bode','Symulacja','Eksperyment','Location','Best')
subplot(2,1,2)
% hold on
semilogx(simulation.freq,-45+simulation.freq*0, 'k', simulation.freq,-135+simulation.freq*0, 'k');
semilogx(simulation.freq, PHASE, 'r',simulation.freq,simulation.phase_deg, 'bx',experiment.freq,experiment.phase_deg, 'gx');
% hold off
% grid on
title('Faza')
ylabel('Faza, stopnie (^o)');
xlabel('Czestotliwość, Hz');
xlim([min(simulation.freq), max(simulation.freq)]);
ylim([-300, 0]);
