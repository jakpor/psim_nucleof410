clear all
% close all
%% Parametry modelu
T0 = 0.001;
% R1 = 24000;
% C1 = 220e-9;
% R2 = 3300;
% C2 = 100e-9;
K = 3.3;
R1 = 4700;C1 = 10e-6;R2 = 22000; C2 = 22e-6;
T1 = (R1*C1); f1 = 1/(2*pi*T1);
T2 = (R2*C2); f2 = 1/(2*pi*T2);

%% Transmitancja modelu
NUM = 1;
DEN = [T1*T2, T1+T2, 1];
SYS = tf(NUM,DEN);

%% Czestotliwosci
f_p = 100; %Hz - czestotliwosc probkowania
N_FFT = 2^12;
t_p = 1/f_p; % czas probkowania (do generowania sterowania - symualcja dziala na zmiennym kroku)
u_vcc = 3.3; % VCC

f_start = 0.1; %3 Hz
f_end   = 50; %4000 Hz
% f_sweep = linspace(f_start,f_end, 64);
f_sweep = logspace(log10(f_start),log10(f_end), 50);

noise_power = 0.00001;
noise_seed = randi(2^16);
noise_t_p = t_p*2;

%% P�tla
gain_table = zeros(size(f_sweep));
phase_table = zeros(size(f_sweep));
sim_data = cell(size(f_sweep));
for iter = 1%:1:length(f_sweep)
f_temp = f_sweep(iter);
%% Czas symulacji i Wektor sterowa�
t_period = 1/f_temp;
sample_per_period = ceil(t_period/t_p);
n_period = 20;%floor(length(t_part)/sample_per_period);%floor(f_temp*max(t_part));
t_stable = 2*t_period; % czas stabilizacji odpowiedzi

t_end = n_period/(f_temp)+t_stable; % czas symulacji
t = 0:t_p:t_end; % wektor czasu
u = (u_vcc/2*(-cos(f_temp*2*pi*t)+1)); %sinus
% plot(t,u); % Sprawdzenie sinusa

%% Symulacja
simout = sim('czwornik_model_2012','StopTime',num2str(t_end));
%% Zebranie danych
response = simout.get('response');
sym.y = response.signals.values(:,1);
control = simout.get('control');
sym.u = control.signals.values(:,1);
sym.t = response.time();

sim_data{iter} = sym;

%% Analiza danych po symulacji
% figure;
% szer = 1;
% wys = 2;
% subplot(wys, szer, 1);
% plot(sym.t,sym.y);
% title('Odpowied� obiektu');
% subplot(wys, szer, 2);
% plot(sym.t,sym.u);
% title('Sterowanie');

%% Obetnij czas stabilizacji obiektu
u_part = sym.u(t>=t_stable);
y_part = sym.y(t>=t_stable);
t_part = sym.t(t>=t_stable) - t_stable;

%% Wyznaczenie amplitudy drga�
N_FFT = 2^12;
fft_u = fft(u_part,N_FFT);
fft_y = fft(y_part,N_FFT);
% P2 = abs(fft_u/N_FFT); % Amplituda
% P1 = P2(1:N_FFT/2+1); % usu� cz�� ujemn�
% P1(2:end-1) = 2*P1(2:end-1); % Pomn� warto�ci przez 2 (bo ujemne)
fft_f = f_p*(0:(N_FFT/2))/N_FFT;
%plot(fft_f, abs(fft_u(1:L/2+1)), 'gx',fft_f, abs(fft_y(1:L/2+1)), 'rx');
f_temp_nr = find(min(abs(fft_f-f_temp))==abs(fft_f-f_temp));
gain = abs(fft_y(f_temp_nr)*2)/abs(fft_u(f_temp_nr)*2);
mean_phase = angle(fft_y(f_temp_nr)*2)-angle(fft_u(f_temp_nr)*2);

% zakres 0:-2*pi
if(mean_phase>0) 
    mean_phase = mean_phase-2*pi;
end;

%% Zapis wynik�w
gain_table(iter) = gain;
phase_table(iter) = mean_phase;
% display(['f = ', num2str(f_temp), ' mag = ', num2str(gain),' fi = ', num2str(mean_phase)]);
end;
gain_log = 20*log10(gain_table);
phase_deg = 180*phase_table/pi;

%% Wyznaczenie czestotliwosci w�asnych
freq45 = 0;
freq135 = 0;
phase45 = 0;
phase135 = 0;
for i = 1:length(phase_deg)
    if(abs((phase_deg(i)+45))<abs((phase45+45)))
        phase45 = phase_deg(i);
        freq45 = f_sweep(i);
    end
    if(abs((phase_deg(i)+135))<abs((phase135+135)))
        phase135 = phase_deg(i);
        freq135 = f_sweep(i);
    end
end;
display(['phase45  = ', num2str(phase45), '  freq45  = ', num2str(freq45), '  Hz']);
display(['phase135 = ', num2str(phase135), ' freq135 = ', num2str(freq135), ' Hz']);
display(['f1  = ', num2str(f1), ' Hz, f2  = ', num2str(f2), ' Hz']);

%% Create struct
simulation.gain     = gain_table;
simulation.phase    = phase_table;
simulation.gain_log = gain_log;
simulation.phase_deg= phase_deg;
simulation.freq     = f_sweep;
simulation.phase45  = phase45;
simulation.freq45   = freq45;
simulation.phase135 = phase135;
simulation.freq135  = freq135;
save('sim_params.mat','simulation');

