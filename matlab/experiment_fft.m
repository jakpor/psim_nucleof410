clear all
close all

%% import
n_freq = 50;
freq_min = 0.1;
freq_max = 50;
f_sweep = logspace(log10(freq_min), log10(freq_max),n_freq);

gain_table = zeros(n_freq,1);
phase_table = zeros(n_freq,1);
for cnt = 1:n_freq
    name = ['../python_tests/log_',num2str(cnt-1),'.csv'];
    [time_us,u,y]=importlogs(name);

    %%
    sample_time_us = mean(diff(time_us));
    t_p = sample_time_us/1000000;
    f_p = 1/t_p;

    N = 16; % tyle powinno by� sampli
    fy = fft(y, N);
    fu = fft(u, N);
    ff = linspace(0,f_p/2-f_p/N,N/2);
    sample_nr = find(abs((f_p/8) - ff) < 1e-4);
    phase_table(cnt) = (angle(fy(sample_nr))-angle(fu(sample_nr)));
    % zakres 0:-2*pi
    if(phase_table(cnt)>0) 
        phase_table(cnt) = phase_table(cnt)-2*pi;
    end;

    gain_table(cnt) = ((abs(fy(sample_nr))/abs(fu(sample_nr))));

    %display(['f = ', num2str(f_sweep(cnt)), ' mag = ', num2str(gain_table(cnt)),' fi = ', num2str(phase_table(cnt)*180/pi)]);
end;
gain_log = 20*log10(gain_table);
phase_deg = 180*phase_table/pi;

%% wyznaczenie czestotliwosci w�asnych
freq45 = 0;
freq135 = 0;
phase45 = 0;
phase135 = 0;
for i = 1:length(phase_deg)
    if(abs((phase_deg(i)+45))<abs((phase45+45)))
        phase45 = phase_deg(i);
        freq45 = f_sweep(i);
    end
    if(abs((phase_deg(i)+135))<abs((phase135+135)))
        phase135 = phase_deg(i);
        freq135 = f_sweep(i);
    end
end;
% display(['phase45  = ', num2str(phase45), '  freq45  = ', num2str(freq45), '  Hz']);
% display(['phase135 = ', num2str(phase135), ' freq135 = ', num2str(freq135), ' Hz']);

%% Create struct
experiment.gain     = gain_table;
experiment.phase    = phase_table;
experiment.gain_log = gain_log;
experiment.phase_deg= phase_deg;
experiment.freq     = f_sweep;
experiment.phase45  = phase45;
experiment.freq45   = freq45;
experiment.phase135 = phase135;
experiment.freq135  = freq135;
save('exp_params.mat','experiment');
