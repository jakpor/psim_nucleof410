__author__ = 'Kuba'

import numpy as np
import matplotlib.pyplot as plt

f_p = 1 #Hz
f_sin = 0.1 #Hz

N = 30
t = np.linspace(0, 1/f_p*(N-1), N)
y = []
u = []
for i in range(0,N):
    u.append(int(((np.sin(2*np.pi*t[i]*f_sin/f_p))+1)/2 * 1000 + 30))
    y.append(int(((np.sin(2*np.pi*t[i]*f_sin/f_p-0.1))+1)/2 * 1000 + 30))

fy = np.fft.fft(y)
fu = np.fft.fft(u)
ff = np.linspace(0,f_p/2-f_p/N,N//2)
phi = (np.angle(fy[3])-np.angle(fu[3]))*180/np.pi
gain = 20*np.log10((np.abs(fy[3])/np.abs(fu[3])))
# print('fu: {:4.2f}Hz, abs: {:4.2f}, fi: {:4.2f}'.format(ff[3],np.abs(fu[3]), np.angle(fu[3])))
# print('fy: {:4.2f}Hz, abs: {:4.2f}, fi: {:4.2f}'.format(ff[3],np.abs(fy[3]), np.angle(fy[3])))
print('fo: {:4.2f}Hz, abs: {:4.2f}, fi: {:4.2f} deg'.format(ff[3] ,gain, phi))

# plt.stem(ff,abs(fy[0:N//2])*2/(N))
# plt.grid()
# plt.show()
