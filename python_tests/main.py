__author__ = 'Kuba'
# collect data from STM via serial port and write it to multiple log files

import serial
import numpy as np

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='COM4',
    baudrate=2000000,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

# setup frequencies for fft
n_freq      = 50
max_freq    = 1000 #Hz - tim6/32
freq_min    = 0.1
freq_max    = 50
freq_hz     = np.logspace(np.log10(freq_min), np.log10(freq_max), num=n_freq)
freq_table  = 1000/freq_hz
n_samples   = 16
time_max    = 2**32 - 1;

# main loop
if ser.isOpen():
    try:
        for cnt in range(0,n_freq): # for every frequency
            cnt = 45
            ser.flushInput()
            ser.flushOutput()
            ser.write(bytes('d \n',"utf-8")) # start debug
            aaa = 'f {:} \r\n'.format(cnt);
            ser.write(bytes(aaa,"utf-8")) # choose frequency on STM32
            # obsolete data structures
            timestamp   = []
            u           = []
            y           = []
            numOfLines = 0
            f = open('log_{}.csv'.format(cnt), 'w')
            print('Freq {:3.3f} Hz'.format((freq_hz[cnt])))
            ser.flushInput()
            while True:
                response = ser.readline()
                print("read data: " + response.decode('utf-8'), end=' ', flush=True)
                split_val = response.decode('utf-8').split(",")
                if len(split_val) == 3 :
                    timestamp.append(int(split_val[0],base=10))
                    u.append(int(split_val[1],base=10))
                    y.append(int(split_val[2],base=10))
                    # time_us = (timestamp[-1]-timestamp[0])/84
                    time_us = (timestamp[-1]-timestamp[0])/100
                    if (time_us<0):
                        time_us = time_us + time_max/100
                    f.write('{:10.0f},{:6},{:6}\n'.format((time_us), u[-1], y[-1]))
                    numOfLines = numOfLines + 1
                if (numOfLines >= n_samples):
                    break
            ser.write(bytes('s \n',"utf-8")) # stop debug
            f.close()
        ser.close()
    except Exception as e1:
        print("error communicating...: " + str(e1))

else:
    print("cannot open serial port ")
    exit()