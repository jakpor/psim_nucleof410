__author__ = 'Kuba'

import numpy as np

n_freq = 50
max_freq = 1000 #Hz - tim6/32
freq_min = 0.1
freq_max = 50
freq_hz = np.logspace(np.log10(freq_min), np.log10(freq_max), num=n_freq)

# print (freq_hz)

freq_table = 1000/freq_hz
print("Frequency table:")
for i in range(0,n_freq):
    if (i%10==0):
        print('')
    print('{:5}, '.format(int(freq_table[i])), end=' ', flush=True)

print("\nSampling table:")
for i in range(0,n_freq):
    if (i%10==0):
        print('')
    print('{:5}, '.format(int(freq_table[i]/8)), end=' ', flush=True)