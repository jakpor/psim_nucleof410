/*
 * globals.c
 *
 *  Created on: 13.12.2017
 *      Author: Kuba
 */

#include "globals.h"

uint8_t uart_data[100];
uint8_t uart_received = 0; 		// 1 bajt!!
uint16_t size = 0; 			// Rozmiar wysylanej wiadomosci
uint16_t ADC_data = 512;
uint16_t DAC_data = 512;

//tabela dzielnika czestotliwosci (mamy 1000Hz i dzielimy)
uint16_t freq_table[FREQ_NUM] = {
		10000,   8808,   7759,   6835,   6021,   5303,   4672,   4115,   3625,   3193,
		 2813,   2478,   2182,   1922,   1693,   1492,   1314,   1157,   1019,    898,
		  791,    697,    614,    540,    476,    419,    369,    325,    286,    252,
		  222,    196,    172,    152,    134,    118,    104,     91,     80,     71,
		   62,     55,     48,     42,     37,     33,     29,     25,     22,     20
};
// tabela pr�bkowania - pr�bkujemy 8x cz�ciej ni� generowany sinus
uint16_t sampling_rate_table[FREQ_NUM] = {
		 1250,   1101,    969,    854,    752,    662,    584,    514,    453,    399,
		  351,    309,    272,    240,    211,    186,    164,    144,    127,    112,
		   98,     87,     76,     67,     59,     52,     46,     40,     35,     31,
		   27,     24,     21,     19,     16,     14,     13,     11,     10,      8,
		    7,      6,      6,      5,      4,      4,      3,      3,      2,      2,
};

uint16_t freq_nr = 18;
uint16_t current_freq = 1000; //1Hz
uint16_t current_sampling_rate = 100; //10Hz
uint8_t debug_prints = 0;
uint8_t echo_prints = 0;
uint8_t generate_sinus = 1;
uint16_t reference_value = 1;
uint16_t pid_k = 300;  // mnozone razy 1000
uint16_t pid_i = 2000;	// 1/Ti mnozone razy 1000
uint16_t pid_d = 47;	// mnozone razy 1000

// Sinus DAC obci�ty od 0,1V do 3,02V (bo z jakiego� powodu poza zakrezem nie zmienia si�)
uint16_t DAC_SIGNAL_Sinus[DAC_SIGNAL_SINUS_LENGTH] = {
		1878, 2221, 2551, 2855, 3121, 3340, 3503, 3603,
		3637, 3603, 3503, 3340, 3121, 2855, 2551, 2221,
		1878, 1535, 1205,  901,  635,  416,  253,  153,
		 120,  153,  253,  416,  635,  901, 1205, 1535
};

UART_SM_STATE_T UART_SM_STATE = WAIT_FOR_CMD;
UART_COMMANDS_T UART_COMMAND = NONE;

float  y_sample[16] = {};
float  y_fft[16] = {};
float  u_sample[16] = {};
float  u_fft[16] = {};
float y_amp[FREQ_NUM] = {};
float y_phi[FREQ_NUM] = {};
float u_amp[FREQ_NUM] = {};
float u_phi[FREQ_NUM] = {};
float amp[FREQ_NUM] = {};
float phase[FREQ_NUM] = {};
float freq_45 = 0;
float freq_135 = 0;

void update_frequencies(uint16_t nr)
{
	if (nr < FREQ_NUM)
	{
		current_freq=freq_table[nr];
		current_sampling_rate=sampling_rate_table[nr];
		freq_nr = nr;
	}
}

void calculate_pid_coeffs(){
	pid_d = 1000/(2*PI*freq_135);
	pid_i = (2*PI*freq_45*1000);
	pid_k = 1000/(3*PI*freq_45);
}
