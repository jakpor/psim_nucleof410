/**
  ******************************************************************************
  * File Name          : TIM.c
  * Description        : This file provides code for the configuration
  *                      of the TIM instances.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "tim.h"

/* USER CODE BEGIN 0 */
#include <math.h>
#include <stdlib.h>

/* USER CODE END 0 */

TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim11;

/* TIM6 init function */
void MX_TIM6_Init(void)
{
  TIM_MasterConfigTypeDef sMasterConfig;

  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 624;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 4;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}
/* TIM11 init function */
void MX_TIM11_Init(void)
{

  htim11.Instance = TIM11;
  htim11.Init.Prescaler = 999;
  htim11.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim11.Init.Period = 99;
  htim11.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  if (HAL_TIM_Base_Init(&htim11) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM6)
  {
  /* USER CODE BEGIN TIM6_MspInit 0 */

  /* USER CODE END TIM6_MspInit 0 */
    /* TIM6 clock enable */
    __HAL_RCC_TIM6_CLK_ENABLE();

    /* TIM6 interrupt Init */
    HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
  /* USER CODE BEGIN TIM6_MspInit 1 */

  /* USER CODE END TIM6_MspInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM11)
  {
  /* USER CODE BEGIN TIM11_MspInit 0 */

  /* USER CODE END TIM11_MspInit 0 */
    /* TIM11 clock enable */
    __HAL_RCC_TIM11_CLK_ENABLE();

    /* TIM11 interrupt Init */
    HAL_NVIC_SetPriority(TIM1_TRG_COM_TIM11_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(TIM1_TRG_COM_TIM11_IRQn);
  /* USER CODE BEGIN TIM11_MspInit 1 */

  /* USER CODE END TIM11_MspInit 1 */
  }
}

void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM6)
  {
  /* USER CODE BEGIN TIM6_MspDeInit 0 */

  /* USER CODE END TIM6_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM6_CLK_DISABLE();

    /* TIM6 interrupt Deinit */
  /* USER CODE BEGIN TIM6:TIM6_DAC_IRQn disable */
    /**
    * Uncomment the line below to disable the "TIM6_DAC_IRQn" interrupt
    * Be aware, disabling shared interrupt may affect other IPs
    */
    /* HAL_NVIC_DisableIRQ(TIM6_DAC_IRQn); */
  /* USER CODE END TIM6:TIM6_DAC_IRQn disable */

  /* USER CODE BEGIN TIM6_MspDeInit 1 */

  /* USER CODE END TIM6_MspDeInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM11)
  {
  /* USER CODE BEGIN TIM11_MspDeInit 0 */

  /* USER CODE END TIM11_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM11_CLK_DISABLE();

    /* TIM11 interrupt Deinit */
    HAL_NVIC_DisableIRQ(TIM1_TRG_COM_TIM11_IRQn);
  /* USER CODE BEGIN TIM11_MspDeInit 1 */

  /* USER CODE END TIM11_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */

//DAC timer - 32kHz -> efektywne 1kHz
void signal_generator(){
	static volatile uint32_t freq_cnt = 0;
	static volatile uint32_t sin_cnt = 0;
	++freq_cnt;
	if (freq_cnt>=current_freq){ //dzielnik czestotliwosci
		freq_cnt = 0;
		DAC_data = DAC_SIGNAL_Sinus[sin_cnt]; //wybor probki -> logowanie
		HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1,DAC_ALIGN_12B_R,DAC_SIGNAL_Sinus[sin_cnt]); //wybor probki -> DAC
		++sin_cnt;
		if (sin_cnt>=32) { sin_cnt=0; }
	}
}

void autotune_fft(){
	// response fft
	R16SRFFT(y_sample, y_fft);
	y_amp[freq_nr] = abs(y_fft[2]) + abs(y_fft[10]);
	y_phi[freq_nr] = atan(y_fft[10]/y_fft[2])*180/PI;
	R16SRFFT(u_sample, u_fft);
	// control fft
	u_amp[freq_nr] = abs(u_fft[2]) + abs(u_fft[10]);
	u_phi[freq_nr] = atan(u_fft[10]/u_fft[2])*180/PI;
	// calculate amp and phase
	amp[freq_nr] = (y_amp[freq_nr]/u_amp[freq_nr]);
	phase[freq_nr] = (y_phi[freq_nr] - u_phi[freq_nr]);
	if(phase[freq_nr] > 0)
	{
		phase[freq_nr] = phase[freq_nr] - 180;
	}
	// detect 45 deg and 135 deg
	if(freq_45 == 0 && (phase[freq_nr]<(-45)))
	{
		freq_45 = 1000./(float)freq_table[freq_nr];
	}
	if(freq_135 == 0 && (phase[freq_nr]<(-135)))
	{
		freq_135 = 1000./(float)freq_table[freq_nr];
	}
	// print debug
	size = sprintf((char*)uart_data, "F: %4d, A: %4d, P: %6d\r\n",
			freq_nr,
			(int)(amp[freq_nr] * 1000),
			(int)((phase[freq_nr])));
	HAL_UART_Transmit_DMA(&huart2, uart_data, size);
	HAL_UART_Receive_DMA(&huart2, &uart_received, 1);
	// Increment frequency index
	freq_nr ++;
	if(freq_nr >= FREQ_NUM){
		UART_COMMAND = NONE;
		freq_nr = 10;
		calculate_pid_coeffs();
	}
	update_frequencies(freq_nr);
}

//Timer pr�bkuj�cy - 1000Hz
void measure_signal_response(){
	static volatile uint32_t sample_cnt = 0;
	static volatile uint32_t fft_sample_cnt = 0;
	++sample_cnt;
	if (sample_cnt>=current_sampling_rate){ //dzielnik czestotliwosci - 8x mniej ni� czestotliwosc sinusa
		sample_cnt = 0;
		if(UART_COMMAND == AUTOTUNE) {
			// collect data for FFT
			y_sample[fft_sample_cnt] = (float) ADC_data;
			u_sample[fft_sample_cnt] = (float) DAC_data;
			fft_sample_cnt ++;
			if (fft_sample_cnt >= FFT_SIZE){
				// data ready
				autotune_fft();
				fft_sample_cnt = 0;
			}
		}
		else {
			if(debug_prints){
				size = sprintf((char*)uart_data, "%10lu, %4u, %4u\r\n", DWT->CYCCNT,DAC_data,ADC_data);
				HAL_UART_Transmit_DMA(&huart2, uart_data, size);
			}
		}
		HAL_UART_Receive_DMA(&huart2, &uart_received, 1);
	}
}

void pid_discrete(){
	volatile static int32_t  e[3] 		= {0,0,0};
	volatile static float last_u 	= 0;
	volatile static float current_u 	= 0;
	e[2] = e[1];
	e[1] = e[0];
	e[0] = reference_value - ADC_data;
	current_u = last_u + (int)(pid_k*(e[0]-e[1] + (pid_i/100)*e[0]/1000 + (pid_d/1000)*100*(e[0]-2*e[1]+e[2]))/1000);
	last_u = current_u;
	HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1,DAC_ALIGN_12B_R,(uint16_t)current_u); //wybor probki -> DAC
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	static volatile uint8_t divider = 0;
	if(generate_sinus == 1){
		if(htim->Instance == TIM6){
			signal_generator();
		}
		if(htim->Instance == TIM11){
			measure_signal_response();
		}
	}
	else{
		if(htim->Instance == TIM11){
			divider ++;
			if (divider >=10)
			{
				pid_discrete();
			}
		}
	}
}

/* USER CODE END 1 */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
