/*
 * globals.h
 *
 *  Created on: 13.12.2017
 *      Author: Kuba
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "stdint.h"

#define DAC_SIGNAL_SINUS_LENGTH			(32)
#define FREQ_NUM 						(50)
#define PI								(3.14159265359)
#define FFT_SIZE 						(16)

typedef enum UART_SM_STATE_T {
	WAIT_FOR_CMD,
	WAIT_FOR_VALUE
} UART_SM_STATE_T;

typedef enum UART_COMMANDS_T {
	NONE,
	SET_FREQUENCY,
	AUTOTUNE,
	PID_CONTROLLER,
	SET_VALUE
} UART_COMMANDS_T;

extern uint8_t uart_received;
extern uint8_t uart_data[100];
extern uint16_t size;
extern uint16_t ADC_data;
extern uint16_t DAC_data;

extern uint16_t freq_table[FREQ_NUM];
extern uint16_t sampling_rate_table[FREQ_NUM];

extern uint16_t freq_nr;
extern uint16_t current_freq;
extern uint16_t current_sampling_rate;
extern uint8_t debug_prints;
extern uint8_t echo_prints;
extern uint8_t generate_sinus;
extern uint16_t reference_value;
extern uint16_t pid_k;
extern uint16_t pid_i;
extern uint16_t pid_d;

extern uint16_t DAC_SIGNAL_Sinus[DAC_SIGNAL_SINUS_LENGTH];

extern float y_sample[FFT_SIZE];
extern float y_fft[FFT_SIZE];
extern float y_amp[FREQ_NUM];
extern float y_phi[FREQ_NUM];
extern float u_sample[FFT_SIZE];
extern float u_fft[FFT_SIZE];
extern float u_amp[FREQ_NUM];
extern float u_phi[FREQ_NUM];
extern float amp[FREQ_NUM];
extern float phase[FREQ_NUM];
extern float freq_45;
extern float freq_135;

extern UART_SM_STATE_T UART_SM_STATE;
extern UART_COMMANDS_T UART_COMMAND;

extern void R16SRFFT(float input[16],float output[16] );
extern void update_frequencies(uint16_t nr);
extern void calculate_pid_coeffs();

#endif /* GLOBALS_H_ */
